import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

/**
 * 
 * @author Matthieu
 * Classe pour tester la telecommande
 */
public class TestTelecommande {

	@Test
	public void testConstructeurVide() {
		//Creation d'objet
		Telecommande t = new Telecommande();
		//Execution de la methode
		int res = t.getPeripheriques().size();
		//Verification
		assertEquals("La liste devrait etre vide",0,res);
	}
	
	@Test
	public void testAjoutLampe() {
		//Creation d'objet
		Telecommande t = new Telecommande();
		//Execution de la methode
		t.ajouterPeripherique(new Lampe("Test"));
		int res = t.getPeripheriques().size();
		//Verification
		assertEquals("Il devrait y avoir un peripherique",1,res);
	}
	
	@Test
	public void testAjout2Lampes() {
		//Creation d'objet
		Telecommande t = new Telecommande();
		//Execution de la methode
		t.ajouterPeripherique(new Lampe("Test"));
		t.ajouterPeripherique(new Lampe("Test2"));
		int res = t.getPeripheriques().size();
		//Verification
		assertEquals("Il devrait y avoir deux peripheriques",2,res);
	}
	
	@Test
	public void testActiverLampePosition0() {
		//Creation d'objet
		Telecommande t = new Telecommande();
		//Execution de la methode
		t.ajouterPeripherique(new Lampe("Test1"));
		t.ajouterPeripherique(new Lampe("Test2"));
		t.ajouterPeripherique(new Lampe("Test3"));
		t.ajouterPeripherique(new Hifi());
		ArrayList<Peripherique> liste = t.getPeripheriques();
		Lampe res = (Lampe) liste.get(0);
		res.allumer();
		//Verification
		assertEquals("Le peripherique Test1 devrait etre allume",res.isAllume(),true);
	}
	
	@Test
	public void testActiverLampePosition1() {
		//Creation d'objet
		Telecommande t = new Telecommande();
		//Execution de la methode
		t.ajouterPeripherique(new Lampe("Test1"));
		t.ajouterPeripherique(new Lampe("Test2"));
		t.ajouterPeripherique(new Lampe("Test3"));
		t.ajouterPeripherique(new Hifi());
		ArrayList<Peripherique> liste = t.getPeripheriques();
		Lampe res = (Lampe) liste.get(1);
		res.allumer();
		//Verification
		assertEquals("La lampe Test2 devrait etre allumee",res.isAllume(),true);
	}
}
