
public class ChemineeAdapter implements Peripherique {

	private Cheminee c;
	
	public ChemineeAdapter(Cheminee c) {
		this.c = c;
	}
	
	@Override
	public void allumer() {
		c.changerIntensite(c.getLumiere()+10);
	}

	@Override
	public void eteindre() {
		c.changerIntensite(0);
	}
	
	public String toString() {
		return c.toString();
	}
}
