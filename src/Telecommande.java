

import java.util.ArrayList;

public class Telecommande {
	
	private ArrayList<Peripherique> peripheriques;
	
	public Telecommande() {
		this.peripheriques = new ArrayList<Peripherique>();
	}

	public void ajouterPeripherique(Peripherique l) {
		if(l != null) {
			this.peripheriques.add(l);
		}
	}
	
	
	public void activerPeripherique(int indice) {
		if(this.peripheriques.size()>=indice)
			this.peripheriques.get(indice).allumer();
	}
	
	public void desactiverPeripherique(int indice) {
		if(this.peripheriques.size()>=indice)
			this.peripheriques.get(indice).eteindre();
	}
	
	public void activerTout() {
		for(int i=0;i<this.peripheriques.size();i++) {
			this.peripheriques.get(i).allumer();
		}
	}
	
	public String toString() {
		String s = "";
		for(int i=0;i<this.peripheriques.size();i++) {
			s = s+this.peripheriques.get(i).toString()+"\n";
		}
		return s;
	}

	public ArrayList<Peripherique> getPeripheriques() {
		return this.peripheriques;
	}
	
	public int getNombre() {
		return this.peripheriques.size();
	}
}
