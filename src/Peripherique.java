

public interface Peripherique {
	
	void allumer();
	
	void eteindre();
	
	String toString();
	
}
