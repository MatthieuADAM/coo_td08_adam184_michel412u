import thermos.Thermostat;
public class Main {

	public static void main(String args[])
	{
		Telecommande t=new Telecommande();
		
		
		Lampe l1=new Lampe("Lampe1");
		t.ajouterPeripherique(l1);
		
		Hifi h1=new Hifi();
		t.ajouterPeripherique(h1);

		Cheminee c1 = new Cheminee();
		ChemineeAdapter ca1 = new ChemineeAdapter(c1);
		t.ajouterPeripherique(ca1);
		
		Thermostat t2 = new Thermostat();
		ThermostatAdapter th = new ThermostatAdapter(t2);
		t.ajouterPeripherique(th);
		
		TelecommandeGraphique tg=new TelecommandeGraphique(t);
		
		
		
	}
	
}
