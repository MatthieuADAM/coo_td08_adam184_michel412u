import thermos.Thermostat;
public class ThermostatAdapter implements Peripherique {
	private Thermostat t;
	
	public ThermostatAdapter(Thermostat thermostat) {
		this.t = thermostat;
	}

	@Override
	public void allumer() {
		t.monterTemperature();
	}

	@Override
	public void eteindre() {
		t.baisserTemperature();
	}
	
	
}
